#!/bin/bash

BOARD=("." "." "." "." "." "." "." "." ".")
CHAR_TO_USE="X"
OTHER_CHAR="O"
WIN="0"

function print_board {
	echo "${BOARD[@]:0:3}"
	echo "${BOARD[@]:3:3}"
	echo "${BOARD[@]:6:3}"
}

function print_all {
	clear
	print_board
	echo "You're playing as: $CHAR_TO_USE"
}

function check_win {
	echo "CHAR TO USE $OTHER_CHAR"
	echo "BOARD 0 ${BOARD[0]}"
	if [[ 
		# horizontal
		(${BOARD[0]} == $OTHER_CHAR && ${BOARD[1]} == $OTHER_CHAR && ${BOARD[2]} == $OTHER_CHAR) || 
		(${BOARD[3]} == $OTHER_CHAR && ${BOARD[4]} == $OTHER_CHAR && ${BOARD[5]} == $OTHER_CHAR) || 
		(${BOARD[6]} == $OTHER_CHAR && ${BOARD[7]} == $OTHER_CHAR && ${BOARD[8]} == $OTHER_CHAR) || 
		# vertical
		(${BOARD[0]} == $OTHER_CHAR && ${BOARD[3]} == $OTHER_CHAR && ${BOARD[6]} == $OTHER_CHAR) || 
		(${BOARD[1]} == $OTHER_CHAR && ${BOARD[4]} == $OTHER_CHAR && ${BOARD[7]} == $OTHER_CHAR) || 
		(${BOARD[2]} == $OTHER_CHAR && ${BOARD[5]} == $OTHER_CHAR && ${BOARD[8]} == $OTHER_CHAR) || 
		# diagonal
		(${BOARD[0]} == $OTHER_CHAR && ${BOARD[4]} == $OTHER_CHAR && ${BOARD[8]} == $OTHER_CHAR) || 
		(${BOARD[2]} == $OTHER_CHAR && ${BOARD[4]} == $OTHER_CHAR && ${BOARD[6]} == $OTHER_CHAR) 
 ]]
	then
		WIN="1"
	else
		WIN="0"
	fi
}


function switch_players {
	    if [[ $CHAR_TO_USE == "X" ]]
	    then
		    CHAR_TO_USE="O"
		    OTHER_CHAR="X"
	    else
		    CHAR_TO_USE="X"
		    OTHER_CHAR="O"
	    fi
}

function pause {
	    echo "Press ENTER to continue"
	    read ANY
}

function read_input {
	read FIELD
	if [[ $FIELD -le 8 && $FIELD -ge 0 ]]
	then
		if [[ ${BOARD[$FIELD]} == "O" || ${BOARD[$FIELD]} == "X" ]]
		then
			echo "This is place is taken, please select empty place (marked with 0)"
			pause
		else
			BOARD[$FIELD]=$CHAR_TO_USE
			switch_players
		fi
	else
		echo "You can only use numbers between 0 and 8"
		pause
	fi
}

while [[ $WIN -eq "0" ]]
do
	print_all
	read_input
	check_win
done

clear
print_board
echo "Player $OTHER_CHAR wins!"
